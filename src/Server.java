import java.util.HashMap;

public class Server {
    private HashMap<String, User> loginPasswordHashMap = new HashMap<>();
    Middleware middleware;

    public String getPasswordByLogin(String email) {
        User user = loginPasswordHashMap.get(email);
        if (user != null) {
            return user.getPassword();
        }
        return null;
    }

    public void login(String email, String password){
        buildChain();
        middleware.check(email, password);
    }

    private void buildChain(){
        middleware = new ThrottlingMiddleware(1);
        Middleware middleware2 = middleware.linkWith(new UserExistMiddleware(this));
        middleware2.linkWith(new UserCheckRoleMiddleware(this));
    }

    public String getRoleByLogin(String email) {
        User user = loginPasswordHashMap.get(email);
        if (user != null) {
            return user.getRole();
        }
        return null;
    }

    public void addUser(User user) {
        this.loginPasswordHashMap.put(user.getEmail(), user);
    }
}

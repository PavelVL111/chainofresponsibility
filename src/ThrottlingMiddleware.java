public class ThrottlingMiddleware extends Middleware {
    private int requestPerMinute;
    private int requset;
    private final int TIME_INTERVAL = 60000;
    private long currentTime;

    public ThrottlingMiddleware(int requestPerMinute) {
        this.requestPerMinute = requestPerMinute;
        this.currentTime = System.currentTimeMillis();
    }

    @Override
    public boolean check(String email, String password) {
        System.out.println("Checking time period filter");
        if(System.currentTimeMillis() < currentTime + TIME_INTERVAL && requset <= requestPerMinute) {
            requset++;
            System.out.println("Allowable interval of time");
            if (hasNext()){
                return middleware.check(email, password);
            }
            return true;
        }
        if(System.currentTimeMillis() > currentTime + TIME_INTERVAL) {
            requset = 0;
            currentTime = System.currentTimeMillis();
        }
        System.out.println("Not allowable interval of time");
        return false;
    }
}

public class UserCheckRoleMiddleware extends Middleware{

    private Server server;

    public UserCheckRoleMiddleware (Server server) {
        this.server = server;
    }

    @Override
    public boolean check(String email, String password) {
        System.out.println("Checking role filter");
        String roleOfUser = this.server.getRoleByLogin(email);
        if(roleOfUser != null){
            if (roleOfUser.equals("admin")) {
                System.out.println("Hello admin");
            }
            if (roleOfUser.equals("user")) {
                System.out.println("Hello user");
            }
            if (hasNext()){
                middleware.check(email, password);
            }
            return true;
        }
        System.out.println("Incorrect user. Role doesn't set");
        return false;
    }
}

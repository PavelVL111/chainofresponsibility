public class UserExistMiddleware extends Middleware{

    private Server server;

    public UserExistMiddleware(Server server) {
        this.server = server;
    }

    @Override
    public boolean check(String email, String password) {
        System.out.println("Checking is valid user");
        String passwordFromSever = this.server.getPasswordByLogin(email);
        if(passwordFromSever != null && passwordFromSever.equals(password)){
            System.out.println("User exist");
            if (hasNext()){
                middleware.check(email, password);
            }
            return true;
        }
        System.out.println("User doesn't exist");
        return false;
    }
}

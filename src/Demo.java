public class Demo {
    public static void main(String[] args) {
        User user1 = new User("email1", "password1", "user");
        User user2 = new User("email2", "password2", "user");
        User user3 = new User("email3", "password3", "admin");

        Server server = new Server();

        server.addUser(user1);
        server.addUser(user2);
        server.addUser(user3);

        server.login("email1", "password1");
    }
}
